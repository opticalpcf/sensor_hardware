<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.3.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="15" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="13" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Aesthetics&lt;/h3&gt;
This library contiains non-functional items such as logos, build/ordering notes, frame blocks, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CREATIVE_COMMONS">
<description>&lt;h3&gt;Creative Commons License Template&lt;/h3&gt;
&lt;p&gt;CC BY-SA 4.0 License with &lt;a href="https://creativecommons.org/licenses/by-sa/4.0/"&gt;link to license&lt;/a&gt; and placeholder for designer name.&lt;/p&gt;
&lt;p&gt;Devices using:
&lt;ul&gt;&lt;li&gt;FRAME_LEDGER&lt;/li&gt;
&lt;li&gt;FRAME_LETTER&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<text x="-20.32" y="5.08" size="1.778" layer="51" font="vector">Released under the Creative Commons Attribution Share-Alike 4.0 License</text>
<text x="0" y="2.54" size="1.778" layer="51" font="vector"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="11.43" y="0" size="1.778" layer="51" font="vector">Designed by:</text>
</package>
<package name="DUMMY">
<description>&lt;h3&gt;Dummy Footprint&lt;/h3&gt;
&lt;p&gt;NOTHING HERE!!! For when you want a symbol with no package as an option against symbols with a package.&lt;/p&gt;

&lt;p&gt;Devices using:
&lt;ul&gt;&lt;li&gt;BADGERHACK_LOGO&lt;/li&gt;
&lt;li&gt;FRAME-LETTER&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
</package>
</packages>
<symbols>
<symbol name="FRAME-LETTER">
<description>&lt;h3&gt;Schematic Frame - Letter&lt;/h3&gt;
&lt;p&gt;Standard 8.5x11 US Ledger frame&lt;/p&gt;
&lt;p&gt;Devices using&lt;ul&gt;&lt;li&gt;FRAME-LETTER&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="0" y1="185.42" x2="248.92" y2="185.42" width="0.4064" layer="94"/>
<wire x1="248.92" y1="185.42" x2="248.92" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="185.42" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="248.92" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<description>&lt;h3&gt;Schematic Documentation Field&lt;/h3&gt;
&lt;p&gt;Autofilling schematic symbol-layer info including board name, designer, revision, and save date.&lt;/p&gt;
&lt;p&gt;Devices using:
&lt;ul&gt;&lt;li&gt;FRAME-LEDGER&lt;/li&gt;
&lt;li&gt;FRAME-LETTER&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.524" y="17.78" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="15.494" y="17.78" size="2.7432" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="2.54" y="31.75" size="1.9304" layer="94">Released under the Creative Commons</text>
<text x="2.54" y="27.94" size="1.9304" layer="94">Attribution Share-Alike 4.0 License</text>
<text x="2.54" y="24.13" size="1.9304" layer="94"> https://creativecommons.org/licenses/by-sa/4.0/</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Design by:</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-LETTER" prefix="FRAME">
<description>&lt;h3&gt;Schematic Frame - Letter&lt;/h3&gt;
&lt;p&gt;Standard 8.5x11 US Letter frame&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME-LETTER" x="0" y="0"/>
<gate name="V" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="" package="CREATIVE_COMMONS">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NO_PACKAGE" package="DUMMY">
<technologies>
<technology name="">
<attribute name="DESIGNER" value="Nobody" constant="no"/>
<attribute name="VERSION" value="v01" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="optical_tof_lib">
<packages>
<package name="FH33-6S-0.5SH(10)-SLAVE">
<description>Footprint for FH33-6S-0.5SH(10) &lt;a href=https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/FH33-6S-0.5SH(10)/HFS06CT-ND/1305357&gt;(Digikey)&lt;/a&gt;</description>
<wire x1="-2.75" y1="1.65" x2="2.75" y2="1.65" width="0.127" layer="51"/>
<wire x1="2.75" y1="1.65" x2="2.75" y2="-1.65" width="0.127" layer="51"/>
<wire x1="2.75" y1="-1.65" x2="-2.75" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-2.75" y1="-1.65" x2="-2.75" y2="1.65" width="0.127" layer="51"/>
<smd name="6" x="-1.25" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-0.75" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-0.25" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.25" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="0.75" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="1" x="1.25" y="1" dx="0.65" dy="0.3" layer="1" rot="R90"/>
<smd name="0A" x="-2.175" y="-1.075" dx="0.55" dy="0.8" layer="1"/>
<smd name="0B" x="2.175" y="-1.075" dx="0.55" dy="0.8" layer="1"/>
<wire x1="-2.75" y1="1.65" x2="-2.75" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-2.75" y1="-1.65" x2="2.75" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="2.75" y1="-1.65" x2="2.75" y2="1.65" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.65" x2="-2.75" y2="1.65" width="0.2032" layer="21"/>
<text x="-1.524" y="-0.254" size="0.635" layer="27">&gt;Value</text>
<text x="-1.524" y="-1.27" size="0.635" layer="25">&gt;Name</text>
</package>
<package name="PCA9554BPWJ">
<description>Footprint for PCA9554BPWJ GPIO Expander &lt;a href=https://www.nxp.com/docs/en/data-sheet/PCA9554B_PCA9554C.pdf&gt;Datasheet&lt;/a&gt;</description>
<wire x1="-2.5" y1="2.2" x2="-2.5" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.2" x2="2.5" y2="-2.2" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.2" x2="2.5" y2="2.2" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.2" x2="-2.5" y2="2.2" width="0.127" layer="51"/>
<smd name="1" x="-2.375" y="-2.925" dx="0.6" dy="1.35" layer="1"/>
<smd name="8" x="2.375" y="-2.925" dx="0.6" dy="1.35" layer="1"/>
<smd name="16" x="-2.375" y="2.925" dx="0.6" dy="1.35" layer="1"/>
<smd name="9" x="2.375" y="2.925" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="-1.625" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="3" x="-0.975" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="4" x="-0.325" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="5" x="0.325" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="6" x="0.975" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="7" x="1.625" y="-2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="15" x="-1.625" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="14" x="-0.975" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="13" x="-0.325" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="12" x="0.325" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="11" x="0.975" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<smd name="10" x="1.625" y="2.925" dx="0.4" dy="1.35" layer="1"/>
<circle x="-2.1" y="-1.7" radius="0.22360625" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.2" x2="-2.5" y2="2.032" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.032" x2="-2.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.2" x2="2.5" y2="-2.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.2" x2="2.5" y2="2.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.2" x2="-2.5" y2="2.2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.032" x2="-2.5" y2="-2.032" width="0.2032" layer="21"/>
<text x="-1.6" y="-1.1" size="0.635" layer="25">&gt;Name</text>
<text x="-1.6" y="0.6" size="0.635" layer="27">&gt;Value</text>
</package>
<package name="VL6180X">
<description>VL6180X footprint -  &lt;a href=http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/11/28/86/e6/26/44/b3/DM00112632.pdf/files/DM00112632.pdf/jcr:content/translations/en.DM00112632.pdf&gt;Datasheet&lt;/a&gt;</description>
<wire x1="-1.4" y1="-2.4" x2="-1.4" y2="2.4" width="0.127" layer="51"/>
<wire x1="-1.4" y1="2.4" x2="1.4" y2="2.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="2.4" x2="1.4" y2="-2.4" width="0.127" layer="51"/>
<wire x1="1.4" y1="-2.4" x2="-1.4" y2="-2.4" width="0.127" layer="51"/>
<smd name="1" x="-1.3" y="1.875" dx="1" dy="0.55" layer="1"/>
<smd name="2" x="-1.3" y="1.125" dx="1" dy="0.55" layer="1"/>
<smd name="3" x="-1.3" y="0.375" dx="1" dy="0.55" layer="1"/>
<smd name="4" x="-1.3" y="-0.375" dx="1" dy="0.55" layer="1"/>
<smd name="5" x="-1.3" y="-1.125" dx="1" dy="0.55" layer="1"/>
<smd name="6" x="-1.3" y="-1.875" dx="1" dy="0.55" layer="1"/>
<smd name="7" x="1.3" y="-1.875" dx="1" dy="0.55" layer="1"/>
<smd name="8" x="1.3" y="-1.125" dx="1" dy="0.55" layer="1"/>
<smd name="9" x="1.3" y="-0.375" dx="1" dy="0.55" layer="1"/>
<smd name="10" x="1.3" y="0.375" dx="1" dy="0.55" layer="1"/>
<smd name="11" x="1.3" y="1.125" dx="1" dy="0.55" layer="1"/>
<smd name="12" x="1.3" y="1.875" dx="1" dy="0.55" layer="1"/>
<circle x="-0.4" y="1.9" radius="0.1" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="2.4" x2="1.4" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-2.4" x2="1.4" y2="-2.4" width="0.2032" layer="21"/>
<text x="-1.5" y="2.7" size="0.635" layer="25">&gt;Name</text>
<text x="-1.5" y="-3.3" size="0.635" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="VL6180X-CONNECTOR">
<description>Symbol for VL6180X Connector &lt;a href=https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/FH33-6S-0.5SH(10)/HFS06CT-ND/1305357&gt;(Digikey)&lt;/a&gt;</description>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="15.24" x2="-5.08" y2="15.24" width="0.254" layer="94"/>
<pin name="SDA" x="7.62" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="VDD" x="7.62" y="-7.62" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="7.62" y="-12.7" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="SCL" x="7.62" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="GPIO0" x="7.62" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="GPIO1" x="7.62" y="12.7" visible="pin" length="short" rot="R180"/>
<text x="-5.08" y="15.24" size="1.778" layer="95">&gt;Name</text>
<text x="-5.08" y="-17.78" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="PCA9554BPWJ">
<description>Schematic symbol for PCA9554BPWJ GPIO Expander - &lt;a href=https://www.nxp.com/docs/en/data-sheet/PCA9554B_PCA9554C.pdf&gt;Datasheet&lt;/a&gt;</description>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="A0" x="-12.7" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="A1" x="-12.7" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="A2" x="-12.7" y="2.54" visible="pin" length="short" direction="in"/>
<pin name="P0" x="-12.7" y="0" visible="pin" length="short"/>
<pin name="P1" x="-12.7" y="-2.54" visible="pin" length="short"/>
<pin name="P2" x="-12.7" y="-5.08" visible="pin" length="short"/>
<pin name="P3" x="-12.7" y="-7.62" visible="pin" length="short"/>
<pin name="VSS" x="-12.7" y="-10.16" visible="pin" length="short" direction="pwr"/>
<pin name="P4" x="12.7" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="P5" x="12.7" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="P6" x="12.7" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="P7" x="12.7" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="!INT" x="12.7" y="0" visible="pin" length="short" rot="R180"/>
<pin name="SCL" x="12.7" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="SDA" x="12.7" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="VDD" x="12.7" y="7.62" visible="pin" length="short" direction="pwr" rot="R180"/>
<text x="-10.16" y="10.16" size="1.778" layer="95">&gt;Name</text>
<text x="-10.16" y="-15.24" size="1.778" layer="96">&gt;Value</text>
</symbol>
<symbol name="VL6180X">
<description>Schematic for VL6180X - &lt;a href=http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/11/28/86/e6/26/44/b3/DM00112632.pdf/files/DM00112632.pdf/jcr:content/translations/en.DM00112632.pdf&gt;Datasheet&lt;/a&gt;</description>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<pin name="GPIO1" x="-15.24" y="12.7" visible="pin" length="short"/>
<pin name="GPIO0" x="-15.24" y="-2.54" visible="pin" length="short"/>
<pin name="SCL" x="-15.24" y="-7.62" visible="pin" length="short"/>
<pin name="SDA" x="-15.24" y="-12.7" visible="pin" length="short"/>
<pin name="AVDD_VCSEL" x="15.24" y="-7.62" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="AVSS_VCSEL" x="15.24" y="-2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="AVDD" x="15.24" y="2.54" visible="pin" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="15.24" y="12.7" visible="pin" length="short" direction="pwr" rot="R180"/>
<text x="-12.7" y="17.78" size="1.778" layer="95">&gt;Name</text>
<text x="-12.7" y="-20.32" size="1.778" layer="96">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VL6180X-CONNECTOR-SLAVE" prefix="U">
<description>A connector that goes on the slave board (i.e board that has the sensor) in order to connect with a VL6180X sensor through a flex cable &lt;a href=https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/FH33-6S-0.5SH(10)/HFS06CT-ND/1305357&gt;Connector&lt;/a&gt;  &lt;a href=http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/11/28/86/e6/26/44/b3/DM00112632.pdf/files/DM00112632.pdf/jcr:content/translations/en.DM00112632.pdf&gt;VL6180X&lt;/a&gt;</description>
<gates>
<gate name="U1" symbol="VL6180X-CONNECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FH33-6S-0.5SH(10)-SLAVE">
<connects>
<connect gate="U1" pin="GND" pad="6"/>
<connect gate="U1" pin="GPIO0" pad="2"/>
<connect gate="U1" pin="GPIO1" pad="1"/>
<connect gate="U1" pin="SCL" pad="3"/>
<connect gate="U1" pin="SDA" pad="4"/>
<connect gate="U1" pin="VDD" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PCA9554BPWJ" prefix="U">
<description>PCA9554BPWJ GPIO Extender - &lt;a href=https://www.nxp.com/docs/en/data-sheet/PCA9554B_PCA9554C.pdf&gt;Datasheet&lt;/a&gt;</description>
<gates>
<gate name="U1" symbol="PCA9554BPWJ" x="0" y="0"/>
</gates>
<devices>
<device name="PCA9554BPWJ" package="PCA9554BPWJ">
<connects>
<connect gate="U1" pin="!INT" pad="13"/>
<connect gate="U1" pin="A0" pad="1"/>
<connect gate="U1" pin="A1" pad="2"/>
<connect gate="U1" pin="A2" pad="3"/>
<connect gate="U1" pin="P0" pad="4"/>
<connect gate="U1" pin="P1" pad="5"/>
<connect gate="U1" pin="P2" pad="6"/>
<connect gate="U1" pin="P3" pad="7"/>
<connect gate="U1" pin="P4" pad="9"/>
<connect gate="U1" pin="P5" pad="10"/>
<connect gate="U1" pin="P6" pad="11"/>
<connect gate="U1" pin="P7" pad="12"/>
<connect gate="U1" pin="SCL" pad="14"/>
<connect gate="U1" pin="SDA" pad="15"/>
<connect gate="U1" pin="VDD" pad="16"/>
<connect gate="U1" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VL6180X" prefix="U">
<description>Vl6180 Optical Time of Flight Module - &lt;a href=http://www.st.com/content/ccc/resource/technical/document/datasheet/c4/11/28/86/e6/26/44/b3/DM00112632.pdf/files/DM00112632.pdf/jcr:content/translations/en.DM00112632.pdf&gt;Datasheet&lt;/a&gt;</description>
<gates>
<gate name="U1" symbol="VL6180X" x="0" y="0"/>
</gates>
<devices>
<device name="LGA12" package="VL6180X">
<connects>
<connect gate="U1" pin="AVDD" pad="10"/>
<connect gate="U1" pin="AVDD_VCSEL" pad="8"/>
<connect gate="U1" pin="AVSS_VCSEL" pad="9"/>
<connect gate="U1" pin="GND" pad="12"/>
<connect gate="U1" pin="GPIO0" pad="4"/>
<connect gate="U1" pin="GPIO1" pad="1"/>
<connect gate="U1" pin="SCL" pad="5"/>
<connect gate="U1" pin="SDA" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="1206">
<description>&lt;p&gt;&lt;b&gt;Generic 3216 (1206) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-2.4" y1="1.1" x2="2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="-1.1" x2="-2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-2.4" y1="-1.1" x2="-2.4" y2="1.1" width="0.0508" layer="39"/>
<wire x1="2.4" y1="1.1" x2="2.4" y2="-1.1" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4.7UF" prefix="C">
<description>&lt;h3&gt;4.7µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08280"/>
<attribute name="VALUE" value="4.7uF"/>
</technology>
</technologies>
</device>
<device name="-1206-16V-(+80/-20%)" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10300" constant="no"/>
<attribute name="VALUE" value="4.7uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="VDD">
<description>&lt;h3&gt;VDD Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VDD" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VDD" prefix="SUPPLY">
<description>&lt;h3&gt;VDD Voltage Supply&lt;/h3&gt;
&lt;p&gt;Positive voltage supply (traditionally for a CMOS device, D=drain).&lt;/p&gt;</description>
<gates>
<gate name="VDD" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603">
<description>&lt;p&gt;&lt;b&gt;Generic 1608 (0603) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="47KOHM" prefix="R">
<description>&lt;h3&gt;47kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07871"/>
<attribute name="VALUE" value="47k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="SparkFun-Aesthetics" deviceset="FRAME-LETTER" device=""/>
<part name="U1" library="optical_tof_lib" deviceset="VL6180X-CONNECTOR-SLAVE" device=""/>
<part name="U2" library="optical_tof_lib" deviceset="PCA9554BPWJ" device="PCA9554BPWJ"/>
<part name="U3" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U4" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U5" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U6" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U7" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U8" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U9" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="U10" library="optical_tof_lib" deviceset="VL6180X" device="LGA12"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C4" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C7" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="C9" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C10" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C11" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C12" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C14" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C15" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C16" library="SparkFun-Capacitors" deviceset="4.7UF" device="0603" value="4.7uF"/>
<part name="C17" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" value="0.1uF"/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY8" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND11" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R2" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R3" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R4" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R5" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R6" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R7" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="R8" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" value="47k"/>
<part name="SUPPLY12" library="SparkFun-PowerSymbols" deviceset="VDD" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="V" x="147.32" y="0"/>
<instance part="U1" gate="U1" x="10.16" y="99.06"/>
<instance part="U2" gate="U1" x="91.44" y="101.6"/>
<instance part="U3" gate="U1" x="40.64" y="157.48"/>
<instance part="U4" gate="U1" x="99.06" y="157.48"/>
<instance part="U5" gate="U1" x="157.48" y="157.48"/>
<instance part="U6" gate="U1" x="213.36" y="157.48"/>
<instance part="U7" gate="U1" x="43.18" y="60.96"/>
<instance part="U8" gate="U1" x="101.6" y="60.96"/>
<instance part="U9" gate="U1" x="162.56" y="60.96"/>
<instance part="U10" gate="U1" x="215.9" y="60.96"/>
<instance part="C1" gate="G$1" x="66.04" y="152.4"/>
<instance part="C2" gate="G$1" x="124.46" y="152.4"/>
<instance part="C3" gate="G$1" x="182.88" y="152.4"/>
<instance part="C4" gate="G$1" x="238.76" y="152.4"/>
<instance part="C5" gate="G$1" x="68.58" y="55.88"/>
<instance part="C6" gate="G$1" x="127" y="55.88"/>
<instance part="C7" gate="G$1" x="185.42" y="55.88"/>
<instance part="C8" gate="G$1" x="238.76" y="55.88"/>
<instance part="C9" gate="G$1" x="58.42" y="142.24"/>
<instance part="C10" gate="G$1" x="116.84" y="142.24"/>
<instance part="C11" gate="G$1" x="175.26" y="142.24"/>
<instance part="C12" gate="G$1" x="231.14" y="142.24"/>
<instance part="C13" gate="G$1" x="60.96" y="45.72"/>
<instance part="C14" gate="G$1" x="119.38" y="45.72"/>
<instance part="C15" gate="G$1" x="180.34" y="45.72"/>
<instance part="C16" gate="G$1" x="233.68" y="45.72"/>
<instance part="C17" gate="G$1" x="124.46" y="99.06"/>
<instance part="SUPPLY1" gate="VDD" x="60.96" y="177.8"/>
<instance part="SUPPLY2" gate="VDD" x="119.38" y="177.8"/>
<instance part="SUPPLY3" gate="VDD" x="177.8" y="177.8"/>
<instance part="SUPPLY4" gate="VDD" x="233.68" y="177.8"/>
<instance part="SUPPLY5" gate="VDD" x="63.5" y="81.28"/>
<instance part="SUPPLY6" gate="VDD" x="121.92" y="81.28"/>
<instance part="SUPPLY7" gate="VDD" x="185.42" y="81.28"/>
<instance part="SUPPLY8" gate="VDD" x="238.76" y="83.82"/>
<instance part="GND1" gate="1" x="238.76" y="38.1"/>
<instance part="GND2" gate="1" x="182.88" y="38.1"/>
<instance part="GND3" gate="1" x="124.46" y="38.1"/>
<instance part="GND4" gate="1" x="66.04" y="38.1"/>
<instance part="GND5" gate="1" x="63.5" y="134.62"/>
<instance part="GND6" gate="1" x="121.92" y="134.62"/>
<instance part="GND7" gate="1" x="180.34" y="134.62"/>
<instance part="GND8" gate="1" x="236.22" y="134.62"/>
<instance part="GND9" gate="1" x="73.66" y="86.36"/>
<instance part="GND10" gate="1" x="129.54" y="86.36"/>
<instance part="GND11" gate="1" x="25.4" y="78.74"/>
<instance part="SUPPLY9" gate="VDD" x="25.4" y="116.84"/>
<instance part="SUPPLY10" gate="VDD" x="10.16" y="73.66"/>
<instance part="SUPPLY11" gate="VDD" x="10.16" y="177.8"/>
<instance part="R1" gate="G$1" x="10.16" y="162.56" rot="R90"/>
<instance part="R2" gate="G$1" x="76.2" y="162.56" rot="R90"/>
<instance part="R3" gate="G$1" x="137.16" y="162.56" rot="R90"/>
<instance part="R4" gate="G$1" x="193.04" y="162.56" rot="R90"/>
<instance part="R5" gate="G$1" x="10.16" y="66.04" rot="R90"/>
<instance part="R6" gate="G$1" x="81.28" y="66.04" rot="R90"/>
<instance part="R7" gate="G$1" x="142.24" y="66.04" rot="R90"/>
<instance part="R8" gate="G$1" x="195.58" y="66.04" rot="R90"/>
<instance part="SUPPLY12" gate="VDD" x="109.22" y="119.38"/>
</instances>
<busses>
</busses>
<nets>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="U1" pin="SCL"/>
<wire x1="17.78" y1="101.6" x2="27.94" y2="101.6" width="0.1524" layer="91"/>
<label x="27.94" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="U1" pin="SCL"/>
<wire x1="104.14" y1="104.14" x2="114.3" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="U1" pin="SCL"/>
<wire x1="25.4" y1="149.86" x2="5.08" y2="149.86" width="0.1524" layer="91"/>
<label x="5.08" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="U1" pin="SCL"/>
<wire x1="83.82" y1="149.86" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<label x="76.2" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="U1" pin="SCL"/>
<wire x1="142.24" y1="149.86" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
<label x="134.62" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="SCL"/>
<wire x1="198.12" y1="149.86" x2="193.04" y2="149.86" width="0.1524" layer="91"/>
<label x="193.04" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="U1" pin="SCL"/>
<wire x1="27.94" y1="53.34" x2="17.78" y2="53.34" width="0.1524" layer="91"/>
<label x="17.78" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="U1" pin="SCL"/>
<wire x1="86.36" y1="53.34" x2="78.74" y2="53.34" width="0.1524" layer="91"/>
<label x="78.74" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="U1" pin="SCL"/>
<wire x1="147.32" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<label x="139.7" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="U1" pin="SCL"/>
<wire x1="200.66" y1="53.34" x2="195.58" y2="53.34" width="0.1524" layer="91"/>
<label x="195.58" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="U1" pin="SDA"/>
<wire x1="17.78" y1="96.52" x2="27.94" y2="96.52" width="0.1524" layer="91"/>
<label x="27.94" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="U1" pin="SDA"/>
<wire x1="104.14" y1="106.68" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<label x="114.3" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="U1" pin="SDA"/>
<wire x1="25.4" y1="144.78" x2="5.08" y2="144.78" width="0.1524" layer="91"/>
<label x="5.08" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="U1" pin="SDA"/>
<wire x1="83.82" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<label x="76.2" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="U1" pin="SDA"/>
<wire x1="142.24" y1="144.78" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<label x="134.62" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="SDA"/>
<wire x1="198.12" y1="144.78" x2="193.04" y2="144.78" width="0.1524" layer="91"/>
<label x="193.04" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="U1" pin="SDA"/>
<wire x1="27.94" y1="48.26" x2="17.78" y2="48.26" width="0.1524" layer="91"/>
<label x="17.78" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="U1" pin="SDA"/>
<wire x1="86.36" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<label x="78.74" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="U1" pin="SDA"/>
<wire x1="147.32" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<label x="139.7" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="U1" pin="SDA"/>
<wire x1="200.66" y1="48.26" x2="195.58" y2="48.26" width="0.1524" layer="91"/>
<label x="195.58" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U1" gate="U1" pin="VDD"/>
<pinref part="SUPPLY9" gate="VDD" pin="VDD"/>
<wire x1="17.78" y1="91.44" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="25.4" y1="91.44" x2="25.4" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="U1" pin="VDD"/>
<pinref part="SUPPLY12" gate="VDD" pin="VDD"/>
<wire x1="104.14" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="109.22" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="124.46" y1="104.14" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
<wire x1="124.46" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<junction x="109.22" y="109.22"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="SUPPLY11" gate="VDD" pin="VDD"/>
<wire x1="10.16" y1="167.64" x2="10.16" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="U1" pin="AVDD"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="55.88" y1="160.02" x2="60.96" y2="160.02" width="0.1524" layer="91"/>
<wire x1="60.96" y1="160.02" x2="66.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="66.04" y1="160.02" x2="66.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="VDD" pin="VDD"/>
<wire x1="60.96" y1="177.8" x2="60.96" y2="172.72" width="0.1524" layer="91"/>
<junction x="60.96" y="160.02"/>
<pinref part="U3" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="60.96" y1="172.72" x2="60.96" y2="160.02" width="0.1524" layer="91"/>
<wire x1="55.88" y1="149.86" x2="58.42" y2="149.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="149.86" x2="58.42" y2="147.32" width="0.1524" layer="91"/>
<wire x1="58.42" y1="149.86" x2="60.96" y2="149.86" width="0.1524" layer="91"/>
<wire x1="60.96" y1="149.86" x2="60.96" y2="160.02" width="0.1524" layer="91"/>
<junction x="58.42" y="149.86"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="76.2" y1="167.64" x2="76.2" y2="172.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="172.72" x2="60.96" y2="172.72" width="0.1524" layer="91"/>
<junction x="60.96" y="172.72"/>
</segment>
<segment>
<pinref part="U4" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY2" gate="VDD" pin="VDD"/>
<wire x1="114.3" y1="160.02" x2="119.38" y2="160.02" width="0.1524" layer="91"/>
<wire x1="119.38" y1="160.02" x2="119.38" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="119.38" y1="172.72" x2="119.38" y2="177.8" width="0.1524" layer="91"/>
<wire x1="119.38" y1="160.02" x2="124.46" y2="160.02" width="0.1524" layer="91"/>
<wire x1="124.46" y1="160.02" x2="124.46" y2="157.48" width="0.1524" layer="91"/>
<junction x="119.38" y="160.02"/>
<pinref part="U4" gate="U1" pin="AVDD_VCSEL"/>
<wire x1="114.3" y1="149.86" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="149.86" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="119.38" y1="149.86" x2="119.38" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="116.84" y1="147.32" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<junction x="116.84" y="149.86"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="137.16" y1="167.64" x2="137.16" y2="172.72" width="0.1524" layer="91"/>
<wire x1="137.16" y1="172.72" x2="119.38" y2="172.72" width="0.1524" layer="91"/>
<junction x="119.38" y="172.72"/>
</segment>
<segment>
<pinref part="U5" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY3" gate="VDD" pin="VDD"/>
<wire x1="172.72" y1="160.02" x2="177.8" y2="160.02" width="0.1524" layer="91"/>
<wire x1="177.8" y1="160.02" x2="177.8" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="177.8" y1="172.72" x2="177.8" y2="177.8" width="0.1524" layer="91"/>
<wire x1="177.8" y1="160.02" x2="182.88" y2="160.02" width="0.1524" layer="91"/>
<wire x1="182.88" y1="160.02" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
<junction x="177.8" y="160.02"/>
<pinref part="U5" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="172.72" y1="149.86" x2="175.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="175.26" y1="149.86" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="175.26" y1="149.86" x2="177.8" y2="149.86" width="0.1524" layer="91"/>
<wire x1="177.8" y1="149.86" x2="177.8" y2="160.02" width="0.1524" layer="91"/>
<junction x="175.26" y="149.86"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="193.04" y1="167.64" x2="193.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="193.04" y1="172.72" x2="177.8" y2="172.72" width="0.1524" layer="91"/>
<junction x="177.8" y="172.72"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY4" gate="VDD" pin="VDD"/>
<wire x1="228.6" y1="160.02" x2="233.68" y2="160.02" width="0.1524" layer="91"/>
<wire x1="233.68" y1="160.02" x2="233.68" y2="177.8" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="233.68" y1="160.02" x2="238.76" y2="160.02" width="0.1524" layer="91"/>
<wire x1="238.76" y1="160.02" x2="238.76" y2="157.48" width="0.1524" layer="91"/>
<junction x="233.68" y="160.02"/>
<pinref part="U6" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="228.6" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="149.86" x2="231.14" y2="147.32" width="0.1524" layer="91"/>
<wire x1="231.14" y1="149.86" x2="233.68" y2="149.86" width="0.1524" layer="91"/>
<wire x1="233.68" y1="149.86" x2="233.68" y2="160.02" width="0.1524" layer="91"/>
<junction x="231.14" y="149.86"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="VDD" pin="VDD"/>
<wire x1="10.16" y1="71.12" x2="10.16" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY5" gate="VDD" pin="VDD"/>
<wire x1="58.42" y1="63.5" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="63.5" y1="76.2" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="63.5" y1="63.5" x2="68.58" y2="63.5" width="0.1524" layer="91"/>
<wire x1="68.58" y1="63.5" x2="68.58" y2="60.96" width="0.1524" layer="91"/>
<junction x="63.5" y="63.5"/>
<pinref part="U7" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="58.42" y1="53.34" x2="60.96" y2="53.34" width="0.1524" layer="91"/>
<wire x1="60.96" y1="53.34" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="60.96" y="53.34"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="81.28" y1="71.12" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<wire x1="81.28" y1="76.2" x2="63.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="63.5" y="76.2"/>
</segment>
<segment>
<pinref part="U8" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY6" gate="VDD" pin="VDD"/>
<wire x1="116.84" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="121.92" y1="76.2" x2="121.92" y2="81.28" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="127" y2="60.96" width="0.1524" layer="91"/>
<junction x="121.92" y="63.5"/>
<pinref part="U8" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="116.84" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<wire x1="119.38" y1="53.34" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="119.38" y1="53.34" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<wire x1="121.92" y1="53.34" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<junction x="119.38" y="53.34"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="142.24" y1="71.12" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<junction x="121.92" y="76.2"/>
</segment>
<segment>
<pinref part="U9" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY7" gate="VDD" pin="VDD"/>
<wire x1="177.8" y1="63.5" x2="180.34" y2="63.5" width="0.1524" layer="91"/>
<wire x1="180.34" y1="63.5" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="185.42" y1="63.5" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="60.96" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<junction x="185.42" y="63.5"/>
<pinref part="U9" gate="U1" pin="AVDD_VCSEL"/>
<wire x1="177.8" y1="53.34" x2="180.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="180.34" y1="53.34" x2="180.34" y2="63.5" width="0.1524" layer="91"/>
<junction x="180.34" y="63.5"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="180.34" y1="50.8" x2="180.34" y2="53.34" width="0.1524" layer="91"/>
<junction x="180.34" y="53.34"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="195.58" y1="71.12" x2="195.58" y2="76.2" width="0.1524" layer="91"/>
<wire x1="195.58" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="185.42" y="76.2"/>
</segment>
<segment>
<pinref part="U10" gate="U1" pin="AVDD"/>
<pinref part="SUPPLY8" gate="VDD" pin="VDD"/>
<wire x1="231.14" y1="63.5" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="233.68" y1="63.5" x2="238.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="238.76" y1="63.5" x2="238.76" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="238.76" y1="60.96" x2="238.76" y2="63.5" width="0.1524" layer="91"/>
<junction x="238.76" y="63.5"/>
<pinref part="U10" gate="U1" pin="AVDD_VCSEL"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="231.14" y1="53.34" x2="233.68" y2="53.34" width="0.1524" layer="91"/>
<wire x1="233.68" y1="53.34" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="53.34" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
<junction x="233.68" y="53.34"/>
<junction x="233.68" y="63.5"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="U1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="17.78" y1="86.36" x2="25.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="25.4" y1="86.36" x2="25.4" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="U1" pin="A0"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="78.74" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="73.66" y1="109.22" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U2" gate="U1" pin="A1"/>
<wire x1="73.66" y1="106.68" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="73.66" y1="104.14" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="78.74" y1="106.68" x2="73.66" y2="106.68" width="0.1524" layer="91"/>
<junction x="73.66" y="106.68"/>
<pinref part="U2" gate="U1" pin="A2"/>
<wire x1="78.74" y1="104.14" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<junction x="73.66" y="104.14"/>
<pinref part="U2" gate="U1" pin="VSS"/>
<wire x1="78.74" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<junction x="73.66" y="91.44"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="124.46" y1="96.52" x2="124.46" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="124.46" y1="93.98" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<wire x1="129.54" y1="93.98" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="U1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="55.88" y1="170.18" x2="63.5" y2="170.18" width="0.1524" layer="91"/>
<wire x1="63.5" y1="170.18" x2="63.5" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="63.5" y1="154.94" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="139.7" x2="58.42" y2="137.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="137.16" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<junction x="63.5" y="137.16"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="66.04" y1="149.86" x2="66.04" y2="137.16" width="0.1524" layer="91"/>
<wire x1="66.04" y1="137.16" x2="63.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U3" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="55.88" y1="154.94" x2="63.5" y2="154.94" width="0.1524" layer="91"/>
<junction x="63.5" y="154.94"/>
</segment>
<segment>
<pinref part="U4" gate="U1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="114.3" y1="170.18" x2="121.92" y2="170.18" width="0.1524" layer="91"/>
<wire x1="121.92" y1="170.18" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="121.92" y1="154.94" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<wire x1="121.92" y1="139.7" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<wire x1="124.46" y1="149.86" x2="124.46" y2="139.7" width="0.1524" layer="91"/>
<wire x1="124.46" y1="139.7" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
<junction x="121.92" y="139.7"/>
<pinref part="U4" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="114.3" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<junction x="121.92" y="154.94"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="116.84" y1="139.7" x2="121.92" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="U1" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="172.72" y1="170.18" x2="180.34" y2="170.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="170.18" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="180.34" y1="154.94" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<wire x1="180.34" y1="139.7" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="149.86" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="182.88" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<junction x="180.34" y="139.7"/>
<pinref part="U5" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="172.72" y1="154.94" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<junction x="180.34" y="154.94"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="175.26" y1="139.7" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="228.6" y1="170.18" x2="236.22" y2="170.18" width="0.1524" layer="91"/>
<wire x1="236.22" y1="170.18" x2="236.22" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="236.22" y1="154.94" x2="236.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="236.22" y1="139.7" x2="236.22" y2="137.16" width="0.1524" layer="91"/>
<wire x1="238.76" y1="149.86" x2="238.76" y2="139.7" width="0.1524" layer="91"/>
<wire x1="238.76" y1="139.7" x2="236.22" y2="139.7" width="0.1524" layer="91"/>
<junction x="236.22" y="139.7"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="231.14" y1="139.7" x2="236.22" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U6" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="228.6" y1="154.94" x2="236.22" y2="154.94" width="0.1524" layer="91"/>
<junction x="236.22" y="154.94"/>
</segment>
<segment>
<pinref part="U7" gate="U1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="58.42" y1="73.66" x2="66.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="66.04" y1="73.66" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="66.04" y1="58.42" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="53.34" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<junction x="66.04" y="43.18"/>
<pinref part="U7" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="58.42" y1="58.42" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="66.04" y="58.42"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="60.96" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="U1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="116.84" y1="73.66" x2="124.46" y2="73.66" width="0.1524" layer="91"/>
<wire x1="124.46" y1="73.66" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="124.46" y1="58.42" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<wire x1="124.46" y1="43.18" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<wire x1="127" y1="53.34" x2="127" y2="43.18" width="0.1524" layer="91"/>
<wire x1="127" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<junction x="124.46" y="43.18"/>
<pinref part="U8" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="116.84" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<junction x="124.46" y="58.42"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="119.38" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U9" gate="U1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="177.8" y1="73.66" x2="182.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="182.88" y1="73.66" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="182.88" y1="58.42" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<wire x1="182.88" y1="43.18" x2="182.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="185.42" y1="53.34" x2="185.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="185.42" y1="43.18" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<junction x="182.88" y="43.18"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="180.34" y1="43.18" x2="182.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U9" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="177.8" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<junction x="182.88" y="58.42"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="233.68" y1="43.18" x2="236.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="236.22" y1="43.18" x2="238.76" y2="43.18" width="0.1524" layer="91"/>
<wire x1="238.76" y1="43.18" x2="238.76" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="238.76" y1="53.34" x2="238.76" y2="43.18" width="0.1524" layer="91"/>
<junction x="238.76" y="43.18"/>
<pinref part="U10" gate="U1" pin="GND"/>
<wire x1="231.14" y1="73.66" x2="236.22" y2="73.66" width="0.1524" layer="91"/>
<wire x1="236.22" y1="73.66" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<junction x="236.22" y="43.18"/>
<pinref part="U10" gate="U1" pin="AVSS_VCSEL"/>
<wire x1="236.22" y1="58.42" x2="236.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="231.14" y1="58.42" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<junction x="236.22" y="58.42"/>
</segment>
</net>
<net name="P0" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P0"/>
<wire x1="78.74" y1="101.6" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<label x="68.58" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="U1" pin="GPIO0"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="154.94" x2="10.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="10.16" y1="154.94" x2="10.16" y2="157.48" width="0.1524" layer="91"/>
<wire x1="10.16" y1="154.94" x2="5.08" y2="154.94" width="0.1524" layer="91"/>
<junction x="10.16" y="154.94"/>
<label x="5.08" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P1"/>
<wire x1="78.74" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<label x="68.58" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="U1" pin="GPIO0"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="154.94" x2="76.2" y2="154.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="154.94" x2="76.2" y2="157.48" width="0.1524" layer="91"/>
<wire x1="76.2" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="76.2" y="154.94"/>
<label x="71.12" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P2"/>
<wire x1="78.74" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<label x="68.58" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="137.16" y1="157.48" x2="137.16" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U5" gate="U1" pin="GPIO0"/>
<wire x1="137.16" y1="154.94" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="137.16" y1="154.94" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<junction x="137.16" y="154.94"/>
<label x="129.54" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P3" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P3"/>
<wire x1="78.74" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<label x="68.58" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U6" gate="U1" pin="GPIO0"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="198.12" y1="154.94" x2="193.04" y2="154.94" width="0.1524" layer="91"/>
<wire x1="193.04" y1="154.94" x2="193.04" y2="157.48" width="0.1524" layer="91"/>
<wire x1="193.04" y1="154.94" x2="187.96" y2="154.94" width="0.1524" layer="91"/>
<junction x="193.04" y="154.94"/>
<label x="187.96" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P7" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P7"/>
<wire x1="104.14" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<label x="114.3" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U7" gate="U1" pin="GPIO0"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="27.94" y1="58.42" x2="10.16" y2="58.42" width="0.1524" layer="91"/>
<wire x1="10.16" y1="58.42" x2="10.16" y2="60.96" width="0.1524" layer="91"/>
<wire x1="10.16" y1="58.42" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="10.16" y="58.42"/>
<label x="2.54" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="P6" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P6"/>
<wire x1="104.14" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<label x="114.3" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="U1" pin="GPIO0"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="86.36" y1="58.42" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<wire x1="81.28" y1="58.42" x2="81.28" y2="60.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="58.42" x2="76.2" y2="58.42" width="0.1524" layer="91"/>
<junction x="81.28" y="58.42"/>
<label x="76.2" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="P5" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P5"/>
<wire x1="104.14" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<label x="114.3" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U9" gate="U1" pin="GPIO0"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="147.32" y1="58.42" x2="142.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="142.24" y1="58.42" x2="142.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="142.24" y1="58.42" x2="137.16" y2="58.42" width="0.1524" layer="91"/>
<junction x="142.24" y="58.42"/>
<label x="137.16" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="P4" class="0">
<segment>
<pinref part="U2" gate="U1" pin="P4"/>
<wire x1="104.14" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="114.3" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U10" gate="U1" pin="GPIO0"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="200.66" y1="58.42" x2="195.58" y2="58.42" width="0.1524" layer="91"/>
<wire x1="195.58" y1="58.42" x2="195.58" y2="60.96" width="0.1524" layer="91"/>
<wire x1="195.58" y1="58.42" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="195.58" y="58.42"/>
<label x="190.5" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
